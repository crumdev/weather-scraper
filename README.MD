# Smart Meter Scraper

A small python script created to scrape the daily details from the LGE-KU Smart Meter dashboard
that contains the Low, Average, and High temperature for the day as well as the cost of your
energy usage.

## Data Source

**Current**: the script requires the user login manually then download the html from the dashboard using
their browsers developer tools.  
**Future**: Script will login and scrape the target on a scheduled basis.

- Logon page:
  - https://my.lge-ku.com/cs/logon.sap
- Dashboard:
  - https://mymeter.lge-ku.com/Dashboard
  - The table with the raw data gets loaded via an Ajax request when you click this link on the page:
    - `<a class="jsAjaxLink" data-js-ajax-link-options="{&quot;url&quot;:&quot;/Dashboard/Table&quot;, &quot;loadingContainer&quot;:&quot;#ajaxContent&quot;, &quot;large&quot;:&quot;true&quot;}" href="#">Data</a>`

## Data Output

**Current**: writes to local json file.  
**Future**: writes new records to a mongodb database for long term storage and analytics.
