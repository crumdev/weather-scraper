from lxml import html
import requests
from datetime import datetime as dt
import re
import json
import csv


with open("./example.html") as f:
    page = f.read()

rawData = html.fromstring(page)


# Get a list of dates, temperatures, and cost for energy use from the smart meter html table
tempScrape = rawData.xpath(
    "//span[@class='popupWeatherLabel'][1]/parent::div/text()")
dateScrape = rawData.xpath(
    "//div[@class='tooltip ComparisonDiv']/div[@class='popupDiv']/b/div[1]/b/text()")
costScrape = rawData.xpath(
    "//div[@class='tooltip ComparisonDiv']/div[@class='popupDiv']/b/div[1]/text()")

# Clean data to remove unwanted characters and empty lists


def TransformWeatherData(temps, dates, costs):
    tempList = [x for x in
                [t.replace('F', '')
                 .replace(' ', '')
                 .replace('Â°', '')
                 .replace('\n', '')
                 for t in temps]
                if x != ['']]

    # Remove end of time range from string like 'Wed, Mar 06, 2019 12:00 AM - 1:00 AM'
    datesAsStrings = [re.sub(
        r'\s-\s+([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])\s*([AaPp][Mm])$', '', d) for d in dates]

    # Convert string like 'Wed, Mar 06, 2019 12:00 AM - 1:00 AM' to dates
    dateList = [dt.strptime(d, '%a, %b %d, %Y %I:%M %p')
                for d in datesAsStrings]

    costList = [re.sub(r'\n\s+$', '', re.sub(r'^\$', '', c))
                for c in costs]

    # Group all three lists into a key value paired dictionary to organize
    # all the data according to it's respective date.
    weatherDict = {}
    weatherDict['dates'] = []
    for i in range(0, len(tempList)):
        weatherDict['dates'].append(
            {
                'day': dt.strftime(dateList[i], '%m/%d/%Y %H:%M'),
                'cost': costList[i],
                'temperature': tempList[i]
            }
        )

    return weatherDict


def LoadWeatherData(payload):
    # Dump the now organized data into a json file for further processing
    with open('example.json', 'w') as outfile:
        json.dump(payload, outfile)

    csv_columns = ['day', 'cost', 'temperature']
    try:
        with open('example.csv', 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in payload['dates']:
                writer.writerow(data)
    except IOError:
        print("I/O error")


if __name__ == "__main__":
    weatherData = TransformWeatherData(tempScrape, dateScrape, costScrape)
    LoadWeatherData(weatherData)
